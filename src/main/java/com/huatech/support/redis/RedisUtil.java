package com.huatech.support.redis;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

public class RedisUtil {

	@Resource JedisPool jedisPool;
	
	public Jedis getResource(){
		return jedisPool.getResource();
	}
	
	public void set(byte[] key, byte[] value, int seconds){
		Jedis jedis = getResource();
		try{
			jedis.set(key, value);
			if(seconds > 0){
				jedis.expire(key, seconds);
			}
		}finally{
			jedis.close();
		}	
	}
	
	public byte[] get(byte[] key){
		Jedis jedis = getResource();
		try{
			return jedis.get(key);
		}finally{
			jedis.close();
		}
	}
	
	public void del(byte[] key){
		Jedis jedis = getResource();
		try{
			jedis.del(key);
		}finally{
			jedis.close();
		}
	}
	
	public List<String> keys(String prefix){
        Jedis jedis = getResource();
        ScanParams scanParams = new ScanParams();
        scanParams.match(prefix + "*");
        scanParams.count(10000);
        try{
	        ScanResult<String> scan = jedis.scan("0", scanParams);
	        return scan.getResult();
        }finally{
        	jedis.close();
        }
	}
	
	
	/******************** hash 操作 start ********************/
	
	public byte[] hget(byte[] key, byte[] field){
		Jedis jedis = getResource();
		try{
			return jedis.hget(key, field);
		}finally{
			jedis.close();
		}
	}
	
	public void hset(byte[] key, byte[] field, byte[] value){
		Jedis jedis = getResource();
		try{
			jedis.hset(key, field, value);
		}finally{
			jedis.close();
		}
	}
	
	public void hdel(byte[] key, byte[] field){
		Jedis jedis = getResource();
		try{
			jedis.hdel(key, field);
		}finally{
			jedis.close();
		}
	}
	
	public Long hlen(byte[] key){
		Jedis jedis = getResource();
		try{
			return jedis.hlen(key);
		}finally{
			jedis.close();
		}
	}
	public Set<byte[]> hkeys(byte[] key){
		Jedis jedis = getResource();
		try{
			return jedis.hkeys(key);
		}finally{
			jedis.close();
		}
	}
	
	public List<byte[]> hvals(byte[] key){
		Jedis jedis = getResource();
		try{
			return jedis.hvals(key);
		}finally{
			jedis.close();
		}
	}
	/******************** hash 操作 end ********************/
	
}
