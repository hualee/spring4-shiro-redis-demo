package com.huatech.core.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.huatech.core.mapper.RoleMapper;
import com.huatech.core.service.RoleService;

/**
 * ServiceImpl:RoleServiceImpl
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
@Service("roleService") 
public class RoleServiceImpl implements RoleService{
	
    @Resource
    private RoleMapper roleMapper;

	public List<String> findByUsername(String username) {
		return roleMapper.findByUsername(username);
	}

}