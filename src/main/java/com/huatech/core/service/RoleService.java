package com.huatech.core.service;

import java.util.List;

/**
 * Service Interface:RoleService
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public interface RoleService {
	
	List<String> findByUsername(String username);

}