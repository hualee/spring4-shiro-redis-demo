package com.huatech.core.service.impl;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.huatech.core.mapper.PermMapper;
import com.huatech.core.service.PermService;

/**
 * ServiceImpl:PermServiceImpl
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
@Service("permService") 
public class PermServiceImpl implements PermService{
	
    @Resource
    private PermMapper permMapper;

	public List<String> findByRoles(Set<String> roles) {
		return permMapper.findByRoles(roles);
	}


}