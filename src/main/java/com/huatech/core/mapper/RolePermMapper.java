package com.huatech.core.mapper;

import com.huatech.core.domain.RolePerm;

/**
 * Dao Interface:RolePermMapper
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public interface RolePermMapper extends BaseMapper<RolePerm> {

}