package com.huatech.core.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 角色权限校验Controller
 * @author lh@erongdu.com
 *
 */
@RestController
public class RolePermissionController {
	
	@RequestMapping("/admin")
	@RequiresRoles("admin")
	public String admin() {
		return "您拥有admin角色";
	}

	@RequestMapping("/rolesOr")
	public String roleOr() {
		return "您拥有其中一个角色";
	}
	
	@RequestMapping("/roleAll")
	public String roleAll() {
		return "您拥有符合条件的所有角色";
	}
	
	@RequestMapping("/user/add")
	@RequiresPermissions("user:add")
	public String addUser() {
		return "您拥有添加用户权限";
	}
	
	@RequestMapping("/user/modify")
	@RequiresPermissions("user:modify")
	public String modifyUser() {
		return "您拥有修改用户权限";
	}
	
	
}
