package com.huatech.core.domain;

import java.io.Serializable;

/**
 * entity:Role
 * 
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public class Role implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private long	id;		
	private String	role;		

	// Constructor
	public Role() {
	}

	/**
	 * full Constructor
	 */
	public Role(long id, String role) {
		this.id = id;
		this.role = role;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	
	@Override
	public String toString() {
		return "Role [" + "id=" + id + ", role=" + role +  "]";
	}
}
