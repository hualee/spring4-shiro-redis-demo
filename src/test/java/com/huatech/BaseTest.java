package com.huatech;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.huatech.core.domain.User;
import com.huatech.core.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试
@ContextConfiguration({"classpath:spring/spring-context.xml"}) //加载配置文件
public class BaseTest {
	
	@Resource
	private UserService userService;
	@Test
	public void testUser() {
		User user = userService.findByUsername("lihua");
		System.err.println(user.toString());
	}
	
}
