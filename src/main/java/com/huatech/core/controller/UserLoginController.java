package com.huatech.core.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.huatech.core.domain.User;
/**
 * 用户登录、退出Controller
 * @author lh@erongdu.com
 *
 */
@RestController
public class UserLoginController {
	
	@RequestMapping(value = "/doLogin", method = RequestMethod.POST)
	public String doLogin(User user) {
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword());
		token.setRememberMe(user.isRememberMe());
		try {
			subject.login(token);
		}catch (Exception e) {
			return e.getMessage();
		}
		return user.getUsername() +", 登录成功";
	}
	
	@RequestMapping("/doLogout")
	public String doLogout() {
		SecurityUtils.getSubject().logout();
		return "您已退出";
	}
	

}
