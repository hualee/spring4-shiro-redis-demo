package com.huatech.core.service;

import com.huatech.core.domain.User;

/**
 * Service Interface:UserService
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public interface UserService {
	
	User get(long id);
	
	User findByUsername(String username);
	
}