package com.huatech.core.mapper;

import com.huatech.core.domain.UserRole;

/**
 * Dao Interface:UserRoleMapper
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}