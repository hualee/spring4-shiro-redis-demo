package com.huatech.core.mapper;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.huatech.core.domain.Perm;

/**
 * Dao Interface:PermMapper
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public interface PermMapper extends BaseMapper<Perm> {
	
	List<String> findByRoles(@Param("roles") Set<String> roles);

}