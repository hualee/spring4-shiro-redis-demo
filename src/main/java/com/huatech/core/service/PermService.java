package com.huatech.core.service;

import java.util.List;
import java.util.Set;

/**
 * Service Interface:PermService
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public interface PermService {
	
	List<String> findByRoles(Set<String> roles);
	

}