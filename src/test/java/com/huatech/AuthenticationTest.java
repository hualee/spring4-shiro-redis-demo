package com.huatech;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.huatech.support.shiro.SystemAuthorizingRealm;
@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试
@ContextConfiguration({"classpath:spring/spring-context.xml"}) //加载配置文件
public class AuthenticationTest {
	
	@Resource SystemAuthorizingRealm realm;
	//SimpleAccountRealm realm = new SimpleAccountRealm();
	
	@Before
	public void before() {
		//realm.addAccount("lihua", "123456","admin", "role");
	}
	
	@Test
	public void testAuthentication() {
		
		// 1、构建SecurityManager环境
		DefaultSecurityManager securityManager = new DefaultSecurityManager();
		securityManager.setRealm(realm);
		
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher("md5");
		matcher.setHashIterations(1);
		realm.setCredentialsMatcher(matcher);
		
		// 2、主体提交认证请求
		SecurityUtils.setSecurityManager(securityManager);
		
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken("lihua", "123456");
		subject.login(token);
		
		org.junit.Assert.assertTrue(subject.isAuthenticated());
		subject.checkRole("admin");
		
	}
	
	public static void main(String[] args) {
		Md5Hash hash = new Md5Hash("123456", ByteSource.Util.bytes("lihua"), 1);
		System.out.println(hash.toString());
	}

}
