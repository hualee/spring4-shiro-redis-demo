package com.huatech.core.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.huatech.core.domain.User;
import com.huatech.core.mapper.UserMapper;
import com.huatech.core.service.UserService;

/**
 * ServiceImpl:UserServiceImpl
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
@Service("userService") 
public class UserServiceImpl implements UserService{
	
    @Resource
    private UserMapper userMapper;

	public User get(long id) {
		return userMapper.get(id);
	}

	public User findByUsername(String username) {
		return userMapper.findByUsername(username);
	}

}