package com.huatech.core.domain;

import java.io.Serializable;

/**
 * entity:User
 * 
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private long	id;		
	private String	username;		
	private String	password;	
	private boolean rememberMe;

	// Constructor
	public User() {
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}


	@Override
	public String toString() {
		return "User [" + "id=" + id + ", username=" + username + ", password=" + password +  "]";
	}
}
