package com.huatech.core.domain;

import java.io.Serializable;

/**
 * entity:Perm
 * 
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public class Perm implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private long	id;		
	private String	permission;		

	// Constructor
	public Perm() {
	}

	/**
	 * full Constructor
	 */
	public Perm(long id, String permission) {
		this.id = id;
		this.permission = permission;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	
	@Override
	public String toString() {
		return "Perm [" + "id=" + id + ", permission=" + permission +  "]";
	}
}
