package com.huatech.core.domain;

import java.io.Serializable;

/**
 * entity:UserRole
 * 
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public class UserRole implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private long	userId;		
	private long	roleId;		

	// Constructor
	public UserRole() {
	}

	/**
	 * full Constructor
	 */
	public UserRole(long userId, long roleId) {
		this.userId = userId;
		this.roleId = roleId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	
	@Override
	public String toString() {
		return "UserRole [" + "userId=" + userId + ", roleId=" + roleId +  "]";
	}
}
