package com.huatech.core.mapper;

import org.apache.ibatis.annotations.Param;

import com.huatech.core.domain.User;

/**
 * Dao Interface:UserMapper
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public interface UserMapper extends BaseMapper<User> {

	User findByUsername(@Param("username") String username);
}