package com.huatech.core.domain;

import java.io.Serializable;

/**
 * entity:RolePerm
 * 
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public class RolePerm implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private long	roleId;		
	private long	permId;		

	// Constructor
	public RolePerm() {
	}

	/**
	 * full Constructor
	 */
	public RolePerm(long roleId, long permId) {
		this.roleId = roleId;
		this.permId = permId;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public long getPermId() {
		return permId;
	}

	public void setPermId(long permId) {
		this.permId = permId;
	}

	
	@Override
	public String toString() {
		return "RolePerm [" + "roleId=" + roleId + ", permId=" + permId +  "]";
	}
}
