#spring4-shiro-redis-demo
#介绍
整合spring4,mybatis,druid,shiro,redis 等框架，实现用户认证和角色、权限鉴权操作。并将用户会话、角色、权限信息放在redis缓存，实现用户会话和其他信息缓存共享。

#shiro认证过程
	1.创建SecurityManager	
	2.主体提交认证
	3.SecurityManager认证	
	4.Authenticator认证
	5.Realm认证

#Shiro内置过滤器
	anon,authBasic,authc,user,logout
	perms,roles,ssl,port

	
