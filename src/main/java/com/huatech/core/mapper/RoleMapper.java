package com.huatech.core.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.huatech.core.domain.Role;

/**
 * Dao Interface:RoleMapper
 * @author lh
 * @version 3.0
 * @date 2019-1-26
 */
public interface RoleMapper extends BaseMapper<Role> {

	List<String> findByUsername(@Param("username") String username);
}